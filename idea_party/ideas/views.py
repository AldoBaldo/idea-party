from django.shortcuts import render
from .models import Idea
from .forms import IdeaForm
from django.shortcuts import redirect


def idea_new(request):
    if request.user.is_authenticated:
        user = request.user
        if request.method == "POST":
            form = IdeaForm(request.POST)
            if form.is_valid():
                idea = form.save(commit=False)
                idea.created_by = user
                idea.save()
                return redirect('index')
        else:
            form = IdeaForm()
        return render(request, 'ideas/idea_new.html', {'form': form})
    else:
        return redirect('index')
