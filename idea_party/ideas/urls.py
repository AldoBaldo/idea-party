from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^new/$', views.idea_new, name="idea_new"),
]
