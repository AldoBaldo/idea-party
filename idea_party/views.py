from django.shortcuts import render
from idea_party.ideas.models import Idea


def index(request):
    context = dict()
    context['recent_ideas'] = Idea.objects.order_by('-created_on')[:5]
    return render(request, 'index.html', context)
